# ======================== #
# ======================== #
# = Created By Anonymous = #
# ======================== #
# ======================== #

# ======================== #
from datetime import datetime
from pydub import AudioSegment
from selenium import webdriver
from stem.control import Controller
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, WebDriverException
# ======================== #
# pip3 install stem pydub SpeechRecognition selenium requests undetected-chromedriver==1.5.2
# ======================== #
import speech_recognition as sr
import undetected_chromedriver as uc
import random, urllib, os, sys, time, requests, urllib, pydub, re, json, stem.process, shutil, glob
# ======================== #
options = uc.ChromeOptions()
options.add_argument("--disable-notifications")
options.add_argument("--headless") # Linux Based System
options.add_argument("--no-sandbox") # Linux Based System
options.add_argument("--window-size=1366,768")
driver = uc.Chrome(options=options)
# ======================== #

# ======================== #
lantak = random.randint(111111111,999999999)
# ======================== #
clonesid = "https://AyamPaha-10@bitbucket.org/ayampaha-01/testo.git"
emailsid = "friendly-pare.20-114-182-61.plesk.page"
password = "LijayaAnli1188@"
namarepo = "testo"
# ======================== #

# ======================== #
def Logo_Awal():
    print("""
# ======================== #
# ======================== #
# = Created By Anonymous = #
# = Welcome Salam Santuy = #
# ======================== #
# ======================== #
    """, flush=True)
# ======================== #

# ======================== #
def Logo_Akhir():
    print("""
# ======================== #
# ======================== #
# = Created By Anonymous = #
# = Goodbye Salam Santuy = #
# ======================== #
# ======================== #
    """, flush=True)
# ======================== #

# ======================== #
def A01(): # Daftar Github
    time.sleep(2)
    print(f"[!] System Alert! Kode : {lantak}", flush=True)
    time.sleep(2)
    print("[!] Tahap 1 Daftar Github Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://github.com/join")
    time.sleep(2)
    data_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="user_login"]')))
    data_input.send_keys(lantak)
    time.sleep(2)
    data_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="user_email"]')))
    data_input.send_keys(f"{lantak}@{emailsid}")
    time.sleep(2)
    data_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="user_password"]')))
    data_input.send_keys(password)
    time.sleep(2)
    print("[!] Captcha Alert! Bypass Dimulai")
    Captcha()
# ======================== #

# ======================== #
def A02(): # Verifikasi Email Pakai Swenson Mail
    time.sleep(2)
    print("[!] Tahap 2 Verifikasi Email Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://email.swenson.my.id")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//div[2]/div[2]/div[3]"))))
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="user"]')))
    your_input.send_keys(f"{lantak}")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='domain']"))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//form[1]/div[2]/div[1]/div[2]/div[1]/a[1]"))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='create']"))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//main[1]/div[1]/div[1]/div[1]"))))
    time.sleep(2)
    WebDriverWait(driver, 50).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, "/html/body/div[1]/div/div[2]/div/main/div[2]/div[2]/iframe")))
    time.sleep(2)
    #text = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, "//a[normalize-space()='Verify your email']")))
    text = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, "/html/body/table/tbody/tr/td/center/table[3]/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td[1]/a")))
    yora = text.get_attribute("href")
    time.sleep(2)
    driver.get(yora)
    time.sleep(2)
    driver.switch_to.default_content()
    time.sleep(2)
    print("[!] Tahap 2 Verifikasi Email Selesai", flush=True)
    time.sleep(2)
    A03()
# ======================== #

# ======================== #
def A03(): # Tahap Import
    time.sleep(2)
    print("[!] Tahap 3 Proses Import Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://github.com/new/import")
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="vcs_url"]')))
    your_input.send_keys(clonesid)
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="repository_name"]')))
    your_input.send_keys(namarepo)
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="repository_visibility_private"]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="new_repository"]/div[4]/button'))))
    time.sleep(9)
    print("[!] Tahap 3 Proses Import Selesai", flush=True)
    time.sleep(2)
    A04()
# ======================== #

# ======================== #
def A04(): # Proses Setup Marketplace
    time.sleep(2)
    print("[!] Tahap 4 Proses Setup Marketplace Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://github.com/marketplace/circleci")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="marketplace-plans-container"]/div[2]/div/form/div/div[1]/button'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="js-pjax-container"]/div/div/div[2]/div[2]/div[5]/form/button'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="js-oauth-authorize-btn"]'))))
    time.sleep(2)
    print("[!] Tahap 4 Proses Setup Marketplace Selesai", flush=True)
    time.sleep(2)
    A05()
# ======================== #

# ======================== #
def A05(): # Proses Setup Dashboard
    time.sleep(2)
    print("[!] Tahap 5 Proses Setup Dashboard Dimulai", flush=True)
    time.sleep(2)
    driver.get(f"https://app.circleci.com/projects/github/{lantak}/setup/")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[1]/div/div[2]/button'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="butao-0"]'))))
    time.sleep(2)
    try:
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[3]'))))
        time.sleep(2)
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]'))))
        time.sleep(2)
        branch = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]/div[2]/div[1]/input')))
        branch.send_keys("master")
        time.sleep(2)
    except Exception as e:
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]'))))
        time.sleep(2)
        branch = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]/div[2]/div[1]/input')))
        branch.send_keys("master")
        time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/div[1]/button'))))
    time.sleep(2)
    print("[!] Tahap 5 Proses Setup Dashboard Selesai", flush=True)
    time.sleep(2)
# ======================== #

# ======================== #
def Captcha(): # Captcha Code
    try:
        time.sleep(2)
        driver.switch_to.default_content()
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="signup_button"]'))))
        time.sleep(2)
        print("[!] Captcha Alert! Bypass Selesai")
        Valuable = False
        time.sleep(2)
        print("[!] Tahap 1 Daftar Github Selesai", flush=True)
        time.sleep(2)
        A02()
    except Exception as e:
        Valuable = True
        if Valuable:
            while True:
                try:
                    time.sleep(2)
                    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="signup-form"]/div[1]/div[3]/iframe')))
                    time.sleep(2)
                    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="fc-iframe-wrap"]')))
                    time.sleep(2)
                    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[5]/span/a[2]"))))
                    time.sleep(2)
                except Exception as e:
                    time.sleep(0)
                driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="audio_download"]'))))
                time.sleep(9)
                list_of_files = glob.glob("*.wav")
                latest_file = max(list_of_files, key=os.path.getctime)
                print(f"[!] Nama File : {latest_file}")
                time.sleep(2)
                path_to_wav = os.path.normpath(os.path.join(os.getcwd(), f"{latest_file}"))
                sample_audio = sr.AudioFile(path_to_wav)
                time.sleep(2)
                r = sr.Recognizer()
                with sample_audio as source:
                    audio = r.record(source)
                key = r.recognize_google(audio)
                print(f"[!] Captcha Alert! Passcode Awal : {key}", flush=True)
                time.sleep(2)
                for r in ((" ", ""), ("-", ""), ("*", "")):
                    key = key.replace(*r)
                print(f"[!] Captcha Alert! Passcode Akhir : {key}", flush=True)
                time.sleep(2)
                data_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="audio_response_field"]')))
                data_input.send_keys(key)
                time.sleep(2)
                data_input.send_keys(Keys.ENTER)
                Captcha()
# ======================== #

# ======================== #
Logo_Awal() # Logo Awal
# ======================== #
A01() # Daftar Github
# ======================== #

# ======================== #
Logo_Akhir() # Jika Loop Dibatasi/Selesai
# ======================== #

# ======================== #
# ======================== #
# = Created By Anonymous = #
# ======================== #
# ======================== #